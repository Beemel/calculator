public class Division {

    public static double divideNumbers() {
        double firstInput= CalculatorInput.firstUserInput();
        double secondInput= CalculatorInput.secondUserInput();
        return firstInput/secondInput;
    }
}
