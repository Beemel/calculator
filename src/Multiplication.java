public class Multiplication {

    public static double multiplyNumbers() {
        double firstInput= CalculatorInput.firstUserInput();
        double secondInput= CalculatorInput.secondUserInput();
        return firstInput*secondInput;
    }
}
