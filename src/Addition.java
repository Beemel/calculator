public class Addition {

    public static double addNumbers() {
        double firstInput= CalculatorInput.firstUserInput();
        double secondInput= CalculatorInput.secondUserInput();

        return firstInput+secondInput;
    }
}
