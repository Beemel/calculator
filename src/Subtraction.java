public class Subtraction {

    public static double subtractNumbers() {
        double firstInput= CalculatorInput.firstUserInput();
        double secondInput= CalculatorInput.secondUserInput();
        return firstInput-secondInput;
    }
}
