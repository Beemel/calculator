import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
class CalculatorTests {


    @Test
    public void firstCalculatorInput_storeFirstInputAsVariable_returnsVariable() {

        //arrange
        double expectedReturn = 7;

        //act
        double actual = CalculatorInput.firstUserInput();

        //assert
        assertEquals(expectedReturn, actual);
    }

    @Test
    public void secondCalculatorInput_storeSecondInputAsVariable_returnsVariable() {

        //arrange
        double expectedReturn = 3;

        //act
        double actual = CalculatorInput.secondUserInput();

        //assert
        assertEquals(expectedReturn, actual);


    }


    @Test
    public void additionTest_willNumbersBeAdded_returnsAddition()  {

        //arrange
                double expectedReturn =  CalculatorInput.firstUserInput()+ CalculatorInput.secondUserInput();

        //act
                double actual = Addition.addNumbers();

        //assert
        assertEquals(expectedReturn, actual);
    }


    @Test
    public void subtractionTest_willNumbersBeSubtracted_returnsSubtraction() {

        //arrange
        double expectedReturn =  CalculatorInput.firstUserInput() -  CalculatorInput.secondUserInput();

        //act
        double actual = Subtraction.subtractNumbers();

        //assert
        assertEquals(expectedReturn, actual);
    }

    @Test
    public void multipyTest_willNumbersBeMultiplied_returnsMultiplication() {

        //arrange
        double expectedReturn = CalculatorInput.firstUserInput() * CalculatorInput.secondUserInput();

        //act
        double actual = Multiplication.multiplyNumbers();

        //assert
        assertEquals(expectedReturn, actual);
    }

    @Test
    public void divideTest_willNumbersBeDivided_returnsDivision() {

        //arrange
        double expectedReturn =  CalculatorInput.firstUserInput() / CalculatorInput.secondUserInput();

        //act
        double actual = Division.divideNumbers();

        //assert
        assertEquals(expectedReturn, actual);
    }
}




